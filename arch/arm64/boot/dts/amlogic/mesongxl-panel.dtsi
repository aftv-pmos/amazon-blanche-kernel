/*
 * arch/arm64/boot/dts/amlogic/mesongxl-panel.dtsi
 *
 * Copyright (C) 2016 Amlogic, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
*/

/ {
	lcd {
		compatible = "amlogic, lcd";
		dev_name = "lcd";
		mode = "tablet";
		status = "okay";
		fr_auto_policy = <0>; /* 0=disable, 1=60/50hz */
		key_valid = <0>;
		resets = <&clock GCLK_IDX_VCLK2_ENCL &clock GCLK_IDX_VCLK2_VENCL>;
		reset-names = "encl","vencl";
		pinctrl-names = "ttl_6bit_hvsync_de_on","ttl_6bit_hvsync_on",
			"ttl_6bit_de_on","ttl_8bit_hvsync_de_on",
			"ttl_8bit_hvsync_on","ttl_8bit_de_on",
			"ttl_6bit_hvsync_de_off","ttl_8bit_hvsync_de_off";
		pinctrl-0 = <&lcd_ttl_rgb_6bit_pins_on &lcd_ttl_de_hvsync_on_pins>;
		pinctrl-1 = <&lcd_ttl_rgb_6bit_pins_on &lcd_ttl_hvsync_on_pins>;
		pinctrl-2 = <&lcd_ttl_rgb_6bit_pins_on &lcd_ttl_de_on_pins>;
		pinctrl-3 = <&lcd_ttl_rgb_8bit_pins_on &lcd_ttl_de_hvsync_on_pins>;
		pinctrl-4 = <&lcd_ttl_rgb_8bit_pins_on &lcd_ttl_hvsync_on_pins>;
		pinctrl-5 = <&lcd_ttl_rgb_8bit_pins_on &lcd_ttl_de_on_pins>;
		pinctrl-6 = <&lcd_ttl_rgb_6bit_pins_off &lcd_ttl_de_hvsync_off_pins>;
		pinctrl-7 = <&lcd_ttl_rgb_8bit_pins_off &lcd_ttl_de_hvsync_off_pins>;

		/* power type:(0=cpu_gpio, 1=pmu_gpio, 2=signal, 3=extern, 0xff=ending) */
		/* power index:(point gpios_index, or extern_index, 0xff=invalid) */
		/* power value:(0=output low, 1=output high, 2=input) */
		/* power delay:(unit in ms) */
		lcd_cpu-gpios = <&gpio GPIODV_28 1>;
		lcd_cpu_gpio_names = "GPIODV_28";

		lcd_0{
			model_name = "LCD720P";
			interface = "ttl";
			basic_setting = <1280 720 1650 750 8 16 9>; /* h_active, v_active, h_period, v_period, lcd_bits, screen_widht, screen_height */
			range_setting = <1550 1750 740 910 60000000 80000000>; /* h_period_min,max, v_period_min,max, pclk_min,max */
			lcd_timing = <40 220 1 5 20 1>; /* hs_width, hs_bp, hs_pol, vs_width, vs_bp, vs_pol */
			clk_attr = <0 0 1 74250000>; /* fr_adj_type(0=clock, 1=htotal, 2=vtotal), clk_ss_level, clk_auto_generate, pixel_clk(unit in Hz) */
			ttl_attr = <0 1 1 0 0>; /** clk_pol, de_valid, hvsync_vaild, rb_swap, bit_swap */
			phy_attr=<3 0 0 0>; /* vswing_level, preemphasis_level, lvds_clk_vswing_level, lvds_clk_preem_level */
			power_on_step = <0 0 1 20 /* panel power on */
					2 0 0 0   /* signal enable */
					0xff 0 0 0>; /* type, index, value, delay */
			power_off_step = <2 0 0 10 /* signal disable */
					0 0 0 100 /* panel power off */
					0xff 0 0 0>; /* type, index, value, delay */
			backlight_index = <0>;
		};
	}; /* end of lcd */

	lcd_extern{
		compatible = "amlogic, lcd_extern";
		dev_name = "lcd_extern";
		status = "okay";
		key_valid = <0>;

		extern_0{
			index = <0>;
			extern_name = "ext_default";
			status = "disabled";
			type = <0>; /* 0=i2c, 1=spi, 2=mipi */
			i2c_address = <0x1c>; /* 7bit i2c address */
			i2c_second_address = <0xff>; /* 7bit i2c address, 0xff for none */
			i2c_bus = "i2c_bus_d";
			cmd_size = <9>;
			/* init on/off: (type, value..., delay), must match cmd_size for every group */
			/* type: 0x00=cmd(bit[3:0]=1 for second_addr), 0x10=gpio, 0xff=ending*/
			/* value: i2c or spi cmd, or gpio index & level, fill 0x0 for no use */
			/* delay: unit ms */
			init_on = <0x00 0x20 0x01 0x02 0x00 0x40 0xFF 0x00 0x00
				0x00 0x80 0x02 0x00 0x40 0x62 0x51 0x73 0x00
				0x00 0x61 0x06 0x00 0x00 0x00 0x00 0x00 0x00
				0x00 0xC1 0x05 0x0F 0x00 0x08 0x70 0x00 0x00
				0x00 0x13 0x01 0x00 0x00 0x00 0x00 0x00 0x00
				0x00 0x3D 0x02 0x01 0x00 0x00 0x00 0x00 0x00
				0x00 0xED 0x0D 0x01 0x00 0x00 0x00 0x00 0x00
				0x00 0x23 0x02 0x00 0x00 0x00 0x00 0x00 0x0A
				0xff 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00>;
			init_off = <0xff 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00>;
		};
		extern_1{
			index = <1>;
			extern_name = "i2c_T5800Q";
			status = "disabled";
			type = <0>; /* 0=i2c, 1=spi, 2=mipi */
			i2c_address = <0x1c>; /* 7bit i2c address */
			i2c_bus = "i2c_bus_d";
		};
	};

	backlight{
		compatible = "amlogic, backlight";
		dev_name = "backlight";
		status = "okay";
		key_valid = <0>;
		pinctrl-names = "pwm_on","pwm_vs_on";
		pinctrl-0 = <&bl_pwm_on_pins>;
		pinctrl-1 = <&bl_pwm_vs_on_pins>;
		/* power index:(point gpios_index, 0xff=invalid) */
		/* power value:(0=output low, 1=output high, 2=input) */
		/* power delay:(unit in ms) */
		bl-gpios = <&gpio GPIOZ_2 1 &gpio GPIODV_29 1>;
		bl_gpio_names = "GPIOZ_2","GPIODV_29";

		backlight_0{
			index = <0>;
			bl_name = "backlight_pwm";
			bl_level_default_uboot_kernel = <100 100>;
			bl_level_attr = <255 10 128 102>; /* max, min, mid, mid_mapping */

			bl_ctrl_method = <1>; /* 1=pwm, 2=pwm_combo */
			bl_power_attr = <0 0 1 200 200>; /* en_gpio_index, on_value, off_value, on_delay, off_delay */

			/* pwm_method: 0=negative, 1=positive */
			/* pwm_freq: pwm_vs: 1~4(vfreq multiple), other pwm: real freq(unit: Hz) */
			/* duty_max, duty_min: unit in % */
			bl_pwm_port = "PWM_B"; /* PWM_A, PWM_B, PWM_C, PWM_D, PWM_VS */
			bl_pwm_attr = <0 180 100 25>; /* pwm_method, pwm_freq, duty_max, duty_min */
			bl_pwm_power = <1 1 10 10>; /* pwm_gpio_index, pwm_gpio_off, pwm_on_delay, pwm_off_delay */
			bl_pwm_en_sequence_reverse = <0>; /* 1 for reverse */
		};
		backlight_1{
			index = <1>;
			bl_name = "backlight_pwm_vs";
			bl_level_default_uboot_kernel = <100 100>;
			bl_level_attr = <255 10 128 102>; /* max, min, mid, mid_mapping */

			bl_ctrl_method = <1>; /* 1=pwm, 2=pwm_combo */
			bl_power_attr = <0 1 0 200 200>; /* en_gpio_index, on_value, off_value, on_delay, off_delay */

			/* pwm_method: 0=negative, 1=positive */
			/* pwm_freq: pwm_vs: 1~4(vfreq multiple), other pwm: real freq(unit: Hz) */
			/* duty_max, duty_min: unit in % */
			bl_pwm_port = "PWM_VS"; /* PWM_A, PWM_B, PWM_C, PWM_D, PWM_VS */
			bl_pwm_attr = <1 2 100 25>; /* pwm_method, pwm_freq, duty_max, duty_min */
			bl_pwm_power = <1 0 10 10>; /* pwm_gpio_index, pwm_off_value, pwm_on_delay, pwm_off_delay */
			bl_pwm_en_sequence_reverse = <0>; /* 1 for reverse */
		};
	};
};/* end of / */

