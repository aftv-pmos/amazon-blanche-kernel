/*
 * TVHDMI char device driver for M6TV chip of AMLOGIC INC.
 *
 * Copyright (C) 2012 AMLOGIC, INC. All Rights Reserved.
 * Author: Rain Zhang <rain.zhang@amlogic.com>
 * Author: Xiaofei Zhu <xiaofei.zhu@amlogic.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the smems of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 */

/* Standard Linux headers */
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/cdev.h>
#include <linux/clk.h>
#include <linux/of.h>
#include <linux/poll.h>
#include <linux/io.h>
#include <linux/suspend.h>
#include <linux/delay.h>

/* Amlogic headers */
#include <linux/amlogic/amports/vframe_provider.h>
#include <linux/amlogic/amports/vframe_receiver.h>
#include <linux/amlogic/tvin/tvin.h>
#include <linux/amlogic/amports/vframe.h>
#include <linux/of_gpio.h>

/* Local include */
#include "hdmirx_drv.h"
#include "hdmi_rx_reg.h"
#include "hdmi_rx_eq.h"

/*------------------------extern function------------------------------*/
static int aml_hdcp22_pm_notify(struct notifier_block *nb,
	unsigned long event, void *dummy);
/*------------------------extern function end------------------------------*/

/*------------------------macro define------------------------------*/
#define TVHDMI_NAME			"hdmirx"
#define TVHDMI_DRIVER_NAME		"hdmirx"
#define TVHDMI_MODULE_NAME		"hdmirx"
#define TVHDMI_DEVICE_NAME		"hdmirx"
#define TVHDMI_CLASS_NAME		"hdmirx"
#define INIT_FLAG_NOT_LOAD		0x80

#define HDMI_DE_REPEAT_DONE_FLAG	0xF0
#define FORCE_YUV			1
#define FORCE_RGB			2
#define DEF_LOG_BUF_SIZE		0 /* (1024*128) */
#define PRINT_TEMP_BUF_SIZE		64 /* 128 */

/* 50ms timer for hdmirx main loop (HDMI_STATE_CHECK_FREQ is 20) */
#define TIMER_STATE_CHECK		(1*HZ/HDMI_STATE_CHECK_FREQ)
/*------------------------macro define end------------------------------*/

/*------------------------variable define------------------------------*/
static unsigned char init_flag;
static dev_t	hdmirx_devno;
static struct class	*hdmirx_clsp;
/* static int open_flage; */
struct hdmirx_dev_s *devp_hdmirx_suspend;
struct device *hdmirx_dev;
struct delayed_work     eq_dwork;
struct workqueue_struct *eq_wq;
struct delayed_work	esm_dwork;
struct workqueue_struct	*esm_wq;
struct delayed_work	repeater_dwork;
struct workqueue_struct	*repeater_wq;
int suspend_pddq = 1;
unsigned int hdmirx_addr_port;
unsigned int hdmirx_data_port;
unsigned int hdmirx_ctrl_port;
/* attr */
static unsigned char *hdmirx_log_buf;
static unsigned int  hdmirx_log_wr_pos;
static unsigned int  hdmirx_log_rd_pos;
static unsigned int  hdmirx_log_buf_size;
unsigned int pwr_sts;
unsigned char *pEdid_buffer;
struct tasklet_struct rx_tasklet;
uint32_t *pd_fifo_buf;
static DEFINE_SPINLOCK(rx_pr_lock);
DECLARE_WAIT_QUEUE_HEAD(query_wait);

int resume_flag = 0;
MODULE_PARM_DESC(resume_flag, "\n resume_flag\n");
module_param(resume_flag, int, 0664);

static int hdmi_yuv444_enable;
module_param(hdmi_yuv444_enable, int, 0664);
MODULE_PARM_DESC(hdmi_yuv444_enable, "hdmi_yuv444_enable");

static int repeat_function;
MODULE_PARM_DESC(repeat_function, "\n repeat_function\n");
module_param(repeat_function, int, 0664);

bool downstream_repeat_support = 1;
MODULE_PARM_DESC(downstream_repeat_support, "\n downstream_repeat_support\n");
module_param(downstream_repeat_support, bool, 0664);

static int force_color_range;
MODULE_PARM_DESC(force_color_range, "\n force_color_range\n");
module_param(force_color_range, int, 0664);

int pc_mode_en;
MODULE_PARM_DESC(pc_mode_en, "\n pc_mode_en\n");
module_param(pc_mode_en, int, 0664);

bool mute_kill_en;
MODULE_PARM_DESC(mute_kill_en, "\n mute_kill_en\n");
module_param(mute_kill_en, bool, 0664);

static bool en_4096_2_3840;
MODULE_PARM_DESC(en_4096_2_3840, "\n en_4096_2_3840\n");
module_param(en_4096_2_3840, bool, 0664);

static int en_4k_2_2k;
MODULE_PARM_DESC(en_4k_2_2k, "\n en_4k_2_2k\n");
module_param(en_4k_2_2k, int, 0664);

int en_4k_timing = 1;
MODULE_PARM_DESC(en_4k_timing, "\n en_4k_timing\n");
module_param(en_4k_timing, int, 0664);

unsigned int hdmirx_addr_port;
unsigned int hdmirx_data_port;
unsigned int hdmirx_ctrl_port;
unsigned int aspect_ratio_val;

struct reg_map reg_maps[][MAP_ADDR_MODULE_NUM] = {
{	/*gxtvbb*/
	{ /* CBUS */
		.phy_addr = 0xc0800000,
		.size = 0xa00000,
	},
	{ /* HIU */
		.phy_addr = 0xC883C000,
		.size = 0x2000,
	},
	{ /* HDMIRX CAPB3 */
		.phy_addr = 0xd0076000,
		.size = 0x2000,
	},
	{ /* HDMIRX SEC AHB */
		.phy_addr = 0xc883e000,
		.size = 0x2000,
	},
	{ /* HDMIRX SEC AHB */
		.phy_addr = 0xda83e000,
		.size = 0x2000,
	},
	{ /* HDMIRX SEC APB4 */
		.phy_addr = 0xc8834400,
		.size = 0x2000,
	},
	{
		.phy_addr = 0xda846000,
		.size = 0x57ba000,
	},
},
{	/*txl*/
	{ /* CBUS */
		.phy_addr = 0xc0800000,
		.size = 0xa00000,
	},
	{ /* HIU */
		.phy_addr = 0xC883C000,
		.size = 0x2000,
	},
	{ /* HDMIRX CAPB3 */
		.phy_addr = 0xd0076000,
		.size = 0x2000,
	},
	{ /* HDMIRX SEC AHB */
		.phy_addr = 0xc883e000,
		.size = 0x2000,
	},
	{ /* HDMIRX SEC AHB */
		.phy_addr = 0xda83e000,
		.size = 0x2000,
	},
	{ /* HDMIRX SEC APB4 */
		.phy_addr = 0xc8834400,
		.size = 0x2000,
	},
	{
		.phy_addr = 0xda846000,
		.size = 0x57ba000,
	},
},
{	/*txlx*/
	{ /* CBUS */
		.phy_addr = 0xffd26000,
		.size = 0xa00000,
	},
	{ /* HIU */
		.phy_addr = 0xff63C000,
		.size = 0x2000,
	},
	{ /* HDMIRX CAPB3 */
		.phy_addr = 0xffe0d000,
		.size = 0x2000,
	},
	{ /* HDMIRX SEC AHB */
		.phy_addr = 0xff63e000,
		.size = 0x2000,
	},
	{ /* HDMIRX SEC AHB */
		.phy_addr = 0,
		.size = 0,
	},
	{ /* HDMIRX SEC APB4 */
		.phy_addr = 0xff634400,
		.size = 0x2000,
	},
	{
		.phy_addr = 0xff646000,
		.size = 0x2000,
	},
},
};

static struct notifier_block aml_hdcp22_pm_notifier = {
	.notifier_call = aml_hdcp22_pm_notify,
};

static const struct of_device_id hdmirx_dt_match[] = {
	{
		.compatible     = "amlogic, hdmirx",
	},
	{},
};

/*------------------------variable define end------------------------------*/


static enum chip_id_e get_chip_id(void)
{
	if (is_meson_txlx_cpu())
		return CHIP_ID_TXLX;
	else if (is_meson_txl_cpu())
		return CHIP_ID_TXL;
	else if (is_meson_gxtvbb_cpu())
		return CHIP_ID_GXTVBB;
	else
		return CHIP_ID_TXLX;
}

/*static int in_reg_maps_idx(unsigned int addr)
{
	int i;

	for (i = 0; i < MAP_ADDR_MODULE_NUM; i++) {
		if ((addr >= reg_maps[rx.chip_id][i].phy_addr) &&
			((addr - reg_maps[rx.chip_id][i].phy_addr)
			< reg_maps[rx.chip_id][i].size)) {
			return i;
		}
	}

	return -1;
}*/

void rx_init_reg_map(void)
{
	int i;

	for (i = 0; i < MAP_ADDR_MODULE_NUM; i++) {
		if ((reg_maps[rx.chip_id][i].phy_addr == 0) ||
			(reg_maps[rx.chip_id][i].size == 0))
			continue;
		reg_maps[rx.chip_id][i].p = ioremap(
			reg_maps[rx.chip_id][i].phy_addr,
			reg_maps[rx.chip_id][i].size);
		if (!reg_maps[rx.chip_id][i].p) {
			rx_pr("hdmirx: failed Mapped PHY: 0x%x , maped:%p\n",
			reg_maps[rx.chip_id][i].phy_addr,
			reg_maps[rx.chip_id][i].p);
		} else {
			reg_maps[rx.chip_id][i].flag = 1;
			rx_pr("hdmirx: Mapped PHY: 0x%x , maped:%p\n",
			reg_maps[rx.chip_id][i].phy_addr,
			reg_maps[rx.chip_id][i].p);
		}
	}
}

static int check_regmap_flag(unsigned int addr)
{
	return 1;

}

bool hdmirx_repeat_support(void)
{
	return repeat_function && downstream_repeat_support;
}

unsigned int rd_reg_hhi(unsigned int offset)
{
	unsigned int addr = offset +
		reg_maps[rx.chip_id][MAP_ADDR_MODULE_HIU].phy_addr;

	return rd_reg(MAP_ADDR_MODULE_HIU, addr);
}

void wr_reg_hhi(unsigned int offset, unsigned int val)
{
	unsigned int addr = offset +
		reg_maps[rx.chip_id][MAP_ADDR_MODULE_HIU].phy_addr;
	wr_reg(MAP_ADDR_MODULE_HIU, addr, val);
}

unsigned int rd_reg(enum map_addr_module_e module, unsigned int reg_addr)
{
	unsigned int val = 0;

	if ((module < MAP_ADDR_MODULE_NUM) && check_regmap_flag(reg_addr))
		val = readl(reg_maps[rx.chip_id][module].p +
		(reg_addr - reg_maps[rx.chip_id][module].phy_addr));
	else
		rx_pr("rd reg %x error\n", reg_addr);
	return val;
}

void wr_reg(enum map_addr_module_e module,
		unsigned int reg_addr, unsigned int val)
{
	if ((module < MAP_ADDR_MODULE_NUM) && check_regmap_flag(reg_addr))
		writel(val, reg_maps[rx.chip_id][module].p +
		(reg_addr - reg_maps[rx.chip_id][module].phy_addr));
	else
		rx_pr("wr reg %x err\n", reg_addr);
}


static unsigned first_bit_set(uint32_t data)
{
	unsigned n = 32;

	if (data != 0) {
		for (n = 0; (data & 1) == 0; n++)
			data >>= 1;
	}
	return n;
}

uint32_t get(uint32_t data, uint32_t mask)
{
	return (data & mask) >> first_bit_set(mask);
}

uint32_t set(uint32_t data, uint32_t mask, uint32_t value)
{
	return ((value << first_bit_set(mask)) & mask) | (data & ~mask);
}

unsigned int rx_get_plug_info(void)
{
	return pwr_sts;
}
EXPORT_SYMBOL(rx_get_plug_info);

void hdmirx_timer_handler(unsigned long arg)
{
	struct hdmirx_dev_s *devp = (struct hdmirx_dev_s *)arg;
	rx_5v_det();
	rx_check_repeat();
	if (rx.open_fg) {
		rx_nosig_monitor();
		hdmirx_hw_monitor();
		rx_pkt_check_content();
	}
	devp->timer.expires = jiffies + TIMER_STATE_CHECK;
	add_timer(&devp->timer);
}

int hdmirx_dec_support(struct tvin_frontend_s *fe, enum tvin_port_e port)
{
	if ((port >= TVIN_PORT_HDMI0) && (port <= TVIN_PORT_HDMI7)) {
		rx_pr("hdmirx support\n");
		return 0;
	} else
		return -1;
}

int hdmirx_dec_open(struct tvin_frontend_s *fe, enum tvin_port_e port)
{
	struct hdmirx_dev_s *devp;

	devp = container_of(fe, struct hdmirx_dev_s, frontend);
	devp->param.port = port;

	/* should enable the adc ref signal for audio pll */
	vdac_enable(1, 0x10);
	hdmirx_hw_init(port);
	/* timer */
	#if 0
	init_timer(&devp->timer);
	devp->timer.data = (ulong)devp;
	devp->timer.function = hdmirx_timer_handler;
	devp->timer.expires = jiffies + TIMER_STATE_CHECK;
	add_timer(&devp->timer);
	#endif
	rx.open_fg = 1;
	rx_pr("%s port:%x ok nosignal:%d\n", __func__, port, rx.no_signal);
	return 0;
}

void hdmirx_dec_start(struct tvin_frontend_s *fe, enum tvin_sig_fmt_e fmt)
{
	struct hdmirx_dev_s *devp;
	struct tvin_parm_s *parm;

	devp = container_of(fe, struct hdmirx_dev_s, frontend);
	parm = &devp->param;
	parm->info.fmt = fmt;
	parm->info.status = TVIN_SIG_STATUS_STABLE;
	rx_pr("%s fmt:%d ok\n", __func__, fmt);
}

void hdmirx_dec_stop(struct tvin_frontend_s *fe, enum tvin_port_e port)
{
	struct hdmirx_dev_s *devp;
	struct tvin_parm_s *parm;

	devp = container_of(fe, struct hdmirx_dev_s, frontend);
	parm = &devp->param;
	/* parm->info.fmt = TVIN_SIG_FMT_NULL; */
	/* parm->info.status = TVIN_SIG_STATUS_NULL; */
	rx_pr("%s ok\n", __func__);
}

void hdmirx_dec_close(struct tvin_frontend_s *fe)
{
	struct hdmirx_dev_s *devp;
	struct tvin_parm_s *parm;

	/* For txl:should disable the adc ref signal for audio pll
	 * For txlx and the follow-up chips,keep enable
	 */
	if (is_meson_txl_cpu())
		vdac_enable(0, 0x10);

	/* open_flage = 0; */
	rx.open_fg = 0;
	devp = container_of(fe, struct hdmirx_dev_s, frontend);
	parm = &devp->param;
	/*del_timer_sync(&devp->timer);*/
	hdmirx_hw_uninit();
	parm->info.fmt = TVIN_SIG_FMT_NULL;
	parm->info.status = TVIN_SIG_STATUS_NULL;
	rx_pr("%s ok\n", __func__);
}

/* interrupt handler */
int hdmirx_dec_isr(struct tvin_frontend_s *fe, unsigned int hcnt64)
{
	struct hdmirx_dev_s *devp;
	struct tvin_parm_s *parm;
	uint32_t avmuteflag;

	devp = container_of(fe, struct hdmirx_dev_s, frontend);
	parm = &devp->param;

	/*prevent spurious pops or noise when pw down*/
	if (rx.state == FSM_SIG_READY) {
		avmuteflag =
			hdmirx_rd_dwc(DWC_PDEC_GCP_AVMUTE) & 0x03;
		if (avmuteflag == 0x02) {
			rx.avmute_skip += 1;
			return TVIN_BUF_SKIP;
		} else
			rx.avmute_skip = 0;
	}

	/* if there is any error or overflow, do some reset, then rerurn -1;*/
	if ((parm->info.status != TVIN_SIG_STATUS_STABLE) ||
	    (parm->info.fmt == TVIN_SIG_FMT_NULL))
		return -1;
	else if (rx.skip > 0)
		return TVIN_BUF_SKIP;
	return 0;
}

static int hdmi_dec_callmaster(enum tvin_port_e port,
	struct tvin_frontend_s *fe)
{
	int status = hdmirx_rd_top(TOP_HPD_PWR5V);
	switch (port) {
	case TVIN_PORT_HDMI0:
		status = status >> (20 + 0) & 0x1;
		break;
	case TVIN_PORT_HDMI1:
		status = status >> (20 + 1) & 0x1;
		break;
	case TVIN_PORT_HDMI2:
		status = status >> (20 + 2) & 0x1;
		break;
	case TVIN_PORT_HDMI3:
	    status = status >> (20 + 3) & 0x1;
	    break;
	default:
		status = 1;
		break;
	}
	return status;

}

static struct tvin_decoder_ops_s hdmirx_dec_ops = {
	.support    = hdmirx_dec_support,
	.open       = hdmirx_dec_open,
	.start      = hdmirx_dec_start,
	.stop       = hdmirx_dec_stop,
	.close      = hdmirx_dec_close,
	.decode_isr = hdmirx_dec_isr,
	.callmaster_det = hdmi_dec_callmaster,
};

bool hdmirx_is_nosig(struct tvin_frontend_s *fe)
{
	bool ret = 0;

	ret = rx_is_nosig();
	return ret;
}

bool hdmirx_fmt_chg(struct tvin_frontend_s *fe)
{
	bool ret = false;
	enum tvin_sig_fmt_e fmt = TVIN_SIG_FMT_NULL;
	struct hdmirx_dev_s *devp;
	struct tvin_parm_s *parm;

	devp = container_of(fe, struct hdmirx_dev_s, frontend);
	parm = &devp->param;
	if (rx_sig_is_ready() == false)
		ret = true;
	else {
		fmt = hdmirx_hw_get_fmt();
		if (fmt != parm->info.fmt) {
			rx_pr("hdmirx fmt: %d --> %d\n",
				parm->info.fmt, fmt);
			parm->info.fmt = fmt;
			ret = true;
		} else
		    ret = false;
	}
	return ret;
}

enum tvin_sig_fmt_e hdmirx_get_fmt(struct tvin_frontend_s *fe)
{
	enum tvin_sig_fmt_e fmt = TVIN_SIG_FMT_NULL;

	fmt = hdmirx_hw_get_fmt();
	return fmt;
}

bool hdmirx_hw_check_frame_skip(void)
{
	if ((rx.state != FSM_SIG_READY) || (rx.skip > 0))
		return true;

	return false;
}

void hdmirx_get_dvi_info(struct tvin_sig_property_s *prop)
{
	prop->dvi_info = rx.pre.sw_dvi;
}

void hdmirx_get_fps_info(struct tvin_sig_property_s *prop)
{
	uint32_t rate = rx.pre.frame_rate;
	rate = rate/100 + (((rate%100)/10 >= 5) ? 1 : 0);
	prop->fps = rate;
}

void hdmirx_get_active_aspect_ratio(struct tvin_sig_property_s *prop)
{
	prop->aspect_ratio = TVIN_ASPECT_NULL;
	if (rx.cur.active_valid) {
		if (rx.cur.active_ratio == 9)
			prop->aspect_ratio = TVIN_ASPECT_4x3;
		else if (rx.cur.active_ratio == 10)
			prop->aspect_ratio = TVIN_ASPECT_16x9;
		else if (rx.cur.active_ratio == 11)
			prop->aspect_ratio = TVIN_ASPECT_14x9;
		else {
			if (rx.cur.picture_ratio == 1)
				prop->aspect_ratio = TVIN_ASPECT_4x3;
			else
				prop->aspect_ratio = TVIN_ASPECT_16x9;
		}

		prop->bar_end_top = rx.cur.bar_end_top;
		prop->bar_start_bottom = rx.cur.bar_start_bottom;
		prop->bar_end_left = rx.cur.bar_end_left;
		prop->bar_start_right = rx.cur.bar_start_right;
	} else {
			if (rx.cur.picture_ratio == 1)
				prop->aspect_ratio = TVIN_ASPECT_4x3;
			else
				prop->aspect_ratio = TVIN_ASPECT_16x9;
	}
	if (aspect_ratio_val != prop->aspect_ratio) {
		aspect_ratio_val = prop->aspect_ratio;
		hotplug_wait_query();
		if (log_level & VIDEO_LOG)
			rx_pr("aspect ratio changed:0x3a4=0x%x,val=%d\n",
				hdmirx_rd_dwc(0x3a4),
				aspect_ratio_val);
	}
}

void hdmirx_set_timing_info(struct tvin_sig_property_s *prop)
{
	enum tvin_sig_fmt_e sig_fmt;
	sig_fmt = hdmirx_hw_get_fmt();
	/*in some PC case, 4096X2160 show in 3840X2160 monitor will
	result in blurred, so adjust hactive to 3840 to show dot by dot*/
	if (en_4k_2_2k) {
		if ((TVIN_SIG_FMT_HDMI_4096_2160_00HZ == sig_fmt) ||
			(TVIN_SIG_FMT_HDMI_3840_2160_00HZ == sig_fmt)) {
			prop->scaling4w = 1920;
			prop->scaling4h = 1080;
		}
	} else if (en_4096_2_3840) {
		if ((TVIN_SIG_FMT_HDMI_4096_2160_00HZ == sig_fmt) &&
			(prop->color_format == TVIN_RGB444)) {
			prop->hs = 128;
			prop->he = 128;
		}
	}
}

int hdmirx_get_connect_info(void)
{
	return pwr_sts;
}
EXPORT_SYMBOL(hdmirx_get_connect_info);

/* see CEA-861-F table-12 and chapter 5.1:
 * By default, RGB pixel data values should be assumed to have
 * the limited range when receiving a CE video format, and the
 * full range when receiving an IT format.
 * CE Video Format: Any Video Format listed in Table 1
 * except the 640x480p Video Format.
 * IT Video Format: Any Video Format that is not a CE Video Format.
 */
static bool is_it_vid_fmt(void)
{
	bool ret = false;

#ifdef CONFIG_AM_HDMI_SCAN_MODE_NODE
	switch (rx.pre.sw_vic) {
	case HDMI_640x480p60:
	case HDMI_800_600:
	case HDMI_1024_768:
	case HDMI_720_400:
	case HDMI_1280_768:
	case HDMI_1280_800:
	case HDMI_1280_960:
	case HDMI_1280_1024:
	case HDMI_1360_768:
	case HDMI_1366_768:
	case HDMI_1600_900:
	case HDMI_1600_1200:
	case HDMI_1920_1200:
	case HDMI_1440_900:
	case HDMI_1400_1050:
	case HDMI_1680_1050:
	case HDMI_640x480p72:
	case HDMI_640x480p75:
	case HDMI_1152X864P75:
	case HDMI_2560_1440:
		ret = true;
		break;
	default:
		ret = false;
		break;
	}
#else
	if ((rx.pre.sw_vic <= HDMI_640x480p60) ||
		(rx.pre.sw_vic >= HDMI_VESA_OFFSET))
		ret = true;
	else
		ret = false;
#endif

	if (log_level & VIDEO_LOG)
		rx_pr("sw_vic: %d, it video format: %d\n", rx.pre.sw_vic, ret);
	return ret;
}

#ifdef CONFIG_AM_HDMI_SCAN_MODE_NODE
/* see CEA-861F page43
 * A Sink should adjust its scan based on the value of S.
 * A Sink would overscan if it received S=1, and underscan
 * if it received S=2. If it receives S=0, then it should
 * overscan for a CE Video Format and underscan for an IT
 * Video Format.
 */
static enum scan_mode_e hdmirx_get_scan_mode(void)
{
	if (rx.state != FSM_SIG_READY)
		return E_UNKNOWN_SCAN;

	if (rx.pre.scan_info == E_OVERSCAN)
		return E_OVERSCAN;
	else if (rx.pre.scan_info == E_UNDERSCAN)
		return E_UNDERSCAN;

	if (is_it_vid_fmt() || rx.pre.it_content)
		return E_UNDERSCAN;
	else if ((rx.pre.hw_vic == HDMI_UNKNOW) &&
		 (rx.pre.sw_vic != HDMI_UNKNOW) &&
		 (rx.pre.sw_vic != HDMI_UNSUPPORT)) {
		/* VESA timing, VIC = 0 */
		if (log_level & VIDEO_LOG)
			rx_pr("under scan for vesa mode\n");
		return E_UNDERSCAN;
	}

	return E_OVERSCAN;
}

static ssize_t scan_mode_show(struct device *dev,
				struct device_attribute *attr,
				char *buf)
{
	return sprintf(buf, "%x\n", hdmirx_get_scan_mode());
}
#endif

void hdmirx_get_color_fmt(struct tvin_sig_property_s *prop)
{
	int format = rx.pre.colorspace;
	#if (DVI_FIXED_TO_RGB)
	if (1 == rx.pre.sw_dvi)
		format = E_COLOR_RGB;
	#endif
	prop->dest_cfmt = TVIN_YUV422;
	switch (format) {
	case E_COLOR_YUV422:
	case E_COLOR_YUV420:
		prop->color_format = TVIN_YUV422;
		break;
	case E_COLOR_YUV444:
		prop->color_format = TVIN_YUV444;
		if (hdmi_yuv444_enable)
			prop->dest_cfmt = TVIN_YUV444;
		break;
	case E_COLOR_RGB:
	default:
		prop->color_format = TVIN_RGB444;
		if (it_content || pc_mode_en)
			prop->dest_cfmt = TVIN_YUV444;
		break;
	}
	if (rx.pre.interlaced == 1)
		prop->dest_cfmt = TVIN_YUV422;

	switch (prop->color_format) {
	case TVIN_YUV444:
	case TVIN_YUV422:
		if (yuv_quant_range == E_YCC_RANGE_LIMIT)
			prop->color_fmt_range = TVIN_YUV_LIMIT;
		else if (yuv_quant_range == E_YCC_RANGE_FULL)
			prop->color_fmt_range = TVIN_YUV_FULL;
		else
			prop->color_fmt_range = TVIN_FMT_RANGE_NULL;
		break;
	case TVIN_RGB444:
		if ((rgb_quant_range == E_RGB_RANGE_FULL) ||
			(1 == rx.pre.sw_dvi))
			prop->color_fmt_range = TVIN_RGB_FULL;
		else if (rgb_quant_range == E_RGB_RANGE_LIMIT)
			prop->color_fmt_range = TVIN_RGB_LIMIT;
		else if (is_it_vid_fmt())
			prop->color_fmt_range = TVIN_RGB_FULL;
		else
			prop->color_fmt_range = TVIN_RGB_LIMIT;
		break;
	default:
		prop->color_fmt_range = TVIN_FMT_RANGE_NULL;
		break;
	}
}

void hdmirx_get_colordepth(struct tvin_sig_property_s *prop)
{
	int ret = rx.pre.colordepth;
	/*
	if (pc_mode_en == 1) {
		if ((rx.pre.sw_vic == HDMI_2160p_60hz_420) ||
			(rx.pre.sw_vic == HDMI_4096p_60hz_420))
			ret = E_COLORDEPTH_8;
	}
	*/
	prop->colordepth = ret;
}

int hdmirx_hw_get_dvi_info(void)
{
	int ret = E_HDMI;

	if (rx.pre.sw_dvi)
		ret = E_DVI;

	return ret;
}

int hdmirx_hw_get_3d_structure(void)
{
	uint8_t ret = 0;
	if (VSI_FORMAT_3D_FORMAT == rx.vsi_info.vd_fmt)
		ret = 1;
	return ret;
}

void hdmirx_get_vsi_info(struct tvin_sig_property_s *prop)
{
	rx_get_vsi_info();

	prop->trans_fmt = TVIN_TFMT_2D;
	prop->dolby_vision = FALSE;
	if (hdmirx_hw_get_3d_structure() == 1) {
		if (rx.vsi_info._3d_structure == 0x1) {
			/* field alternative */
			prop->trans_fmt = TVIN_TFMT_3D_FA;
		} else if (rx.vsi_info._3d_structure == 0x2) {
			/* line alternative */
			prop->trans_fmt = TVIN_TFMT_3D_LA;
		} else if (rx.vsi_info._3d_structure == 0x3) {
			/* side-by-side full */
			prop->trans_fmt = TVIN_TFMT_3D_LRF;
		} else if (rx.vsi_info._3d_structure == 0x4) {
			/* L + depth */
			prop->trans_fmt = TVIN_TFMT_3D_LD;
		} else if (rx.vsi_info._3d_structure == 0x5) {
			/* L + depth + graphics + graphics-depth */
			prop->trans_fmt = TVIN_TFMT_3D_LDGD;
		} else if (rx.vsi_info._3d_structure == 0x6) {
			/* top-and-bot */
			prop->trans_fmt = TVIN_TFMT_3D_TB;
		} else if (rx.vsi_info._3d_structure == 0x8) {
			/* Side-by-Side half */
			switch (rx.vsi_info._3d_ext_data) {
			case 0x5:
				/*Odd/Left picture, Even/Right picture*/
				prop->trans_fmt = TVIN_TFMT_3D_LRH_OLER;
				break;
			case 0x6:
				/*Even/Left picture, Odd/Right picture*/
				prop->trans_fmt = TVIN_TFMT_3D_LRH_ELOR;
				break;
			case 0x7:
				/*Even/Left picture, Even/Right picture*/
				prop->trans_fmt = TVIN_TFMT_3D_LRH_ELER;
				break;
			case 0x4:
				/*Odd/Left picture, Odd/Right picture*/
			default:
				prop->trans_fmt = TVIN_TFMT_3D_LRH_OLOR;
				break;
			}
		}
		if (is_frame_packing())
			prop->trans_fmt = TVIN_TFMT_3D_FP;
		else if (is_alternative())
			prop->trans_fmt = TVIN_TFMT_3D_LA;
	} else {
		if (rx.vsi_info.dolby_vision_sts ==
			DOLBY_VERSION_START)
			prop->dolby_vision = TRUE;
		else if (rx.vsi_info.dolby_vision_sts ==
			DOLBY_VERSION_STOP)
			prop->dolby_vision = FALSE;
		if (log_level & VSI_LOG)
			rx_pr("prop->dolby_vision:%d\n", prop->dolby_vision);
	}
}


void hdmirx_get_repetition_info(struct tvin_sig_property_s *prop)
{
	prop->decimation_ratio = rx.pre.repeat |
			HDMI_DE_REPEAT_DONE_FLAG;
}

void hdmirx_get_hdr_info(struct tvin_sig_property_s *prop)
{
	struct drm_infoframe_st *drmpkt;

	/*check drm packet is attach every VS*/
	uint32_t drm_attach = rx_pkt_chk_attach_drm();

	drmpkt = (struct drm_infoframe_st *)&(rx.drm_info);

	if (drm_attach) {
		rx.hdr_info.hdr_state = HDR_STATE_SET;
		rx_pkt_clr_attach_drm();
	}

	/* hdr data processing */
	switch (rx.hdr_info.hdr_state) {
	case HDR_STATE_NULL:
		/* filter for state, 10*10ms */
		if (rx.hdr_info.hdr_check_cnt++ > 10) {
			prop->hdr_info.hdr_state = HDR_STATE_NULL;
			rx.hdr_info.hdr_check_cnt = 0;
		}
		break;
	case HDR_STATE_GET:
		rx.hdr_info.hdr_check_cnt = 0;
		break;
	case HDR_STATE_SET:
		rx.hdr_info.hdr_check_cnt = 0;
		if (prop->hdr_info.hdr_state != HDR_STATE_GET) {
			#if 0
			/*prop->hdr_info.hdr_data = rx.hdr_info.hdr_data;*/
			#else
			if (rx_pkt_chk_busy_drm())
				break;

			prop->hdr_info.hdr_data.lenght = drmpkt->length;
			prop->hdr_info.hdr_data.eotf = drmpkt->des_u.tp1.eotf;
			prop->hdr_info.hdr_data.metadata_id =
				drmpkt->des_u.tp1.meta_des_id;
			prop->hdr_info.hdr_data.primaries[0].x =
				drmpkt->des_u.tp1.dis_pri_x0;
			prop->hdr_info.hdr_data.primaries[0].y =
				drmpkt->des_u.tp1.dis_pri_y0;
			prop->hdr_info.hdr_data.primaries[1].x =
				drmpkt->des_u.tp1.dis_pri_x1;
			prop->hdr_info.hdr_data.primaries[1].y =
				drmpkt->des_u.tp1.dis_pri_y1;
			prop->hdr_info.hdr_data.primaries[2].x =
				drmpkt->des_u.tp1.dis_pri_x2;
			prop->hdr_info.hdr_data.primaries[2].y =
				drmpkt->des_u.tp1.dis_pri_y2;
			prop->hdr_info.hdr_data.white_points.x =
				drmpkt->des_u.tp1.white_points_x;
			prop->hdr_info.hdr_data.white_points.y =
				drmpkt->des_u.tp1.white_points_y;
			prop->hdr_info.hdr_data.master_lum.x =
				drmpkt->des_u.tp1.max_dislum;
			prop->hdr_info.hdr_data.master_lum.y =
				drmpkt->des_u.tp1.min_dislum;
			prop->hdr_info.hdr_data.mcll =
				drmpkt->des_u.tp1.max_light_lvl;
			prop->hdr_info.hdr_data.mfall =
				drmpkt->des_u.tp1.max_fa_light_lvl;
			#endif

			/* vdin can read current hdr data */
			prop->hdr_info.hdr_state = HDR_STATE_GET;

			/* Rx can get new hdr data */
			rx.hdr_info.hdr_state = HDR_STATE_NULL;
		}
		break;
	default:
		break;
	}
}

void hdmirx_get_sig_property(struct tvin_frontend_s *fe,
	struct tvin_sig_property_s *prop)
{
	hdmirx_get_dvi_info(prop);
	hdmirx_get_colordepth(prop);
	hdmirx_get_fps_info(prop);
	hdmirx_get_color_fmt(prop);
	hdmirx_get_repetition_info(prop);
	hdmirx_set_timing_info(prop);
	hdmirx_get_hdr_info(prop);
	hdmirx_get_vsi_info(prop);
	hdmirx_get_active_aspect_ratio(prop);
	prop->skip_vf_num = 1;
}

bool hdmirx_check_frame_skip(struct tvin_frontend_s *fe)
{
	return hdmirx_hw_check_frame_skip();
}

static struct tvin_state_machine_ops_s hdmirx_sm_ops = {
	.nosig            = hdmirx_is_nosig,
	.fmt_changed      = hdmirx_fmt_chg,
	.get_fmt          = hdmirx_get_fmt,
	.fmt_config       = NULL,
	.adc_cal          = NULL,
	.pll_lock         = NULL,
	.get_sig_propery  = hdmirx_get_sig_property,
	.vga_set_param    = NULL,
	.vga_get_param    = NULL,
	.check_frame_skip = hdmirx_check_frame_skip,
};

static int hdmirx_open(struct inode *inode, struct file *file)
{
	struct hdmirx_dev_s *devp;

	devp = container_of(inode->i_cdev, struct hdmirx_dev_s, cdev);
	file->private_data = devp;
	return 0;
}

static int hdmirx_release(struct inode *inode, struct file *file)
{
	file->private_data = NULL;
	return 0;
}

static long hdmirx_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	long ret = 0;
	/* unsigned int delay_cnt = 0; */
	struct hdmirx_dev_s *devp = NULL;
	void __user *argp = (void __user *)arg;
	uint32_t param = 0, temp_val, temp;
	int size = sizeof(struct pd_infoframe_s);
	struct pd_infoframe_s pkt_info;
	void *srcbuff;

	if (_IOC_TYPE(cmd) != HDMI_IOC_MAGIC) {
		pr_err("%s invalid command: %u\n", __func__, cmd);
		return -ENOSYS;
	}
	srcbuff = &pkt_info;
	devp = file->private_data;
	switch (cmd) {
	case HDMI_IOC_HDCP_GET_KSV:{
		struct _hdcp_ksv ksv;
		if (argp == NULL)
			return -ENOSYS;
		mutex_lock(&devp->rx_lock);
		ksv.bksv0 = rx.hdcp.bksv[0];
		ksv.bksv1 = rx.hdcp.bksv[1];
		if (copy_to_user(argp, &ksv,
			sizeof(struct _hdcp_ksv))) {
			ret = -EFAULT;
			break;
		}
		mutex_unlock(&devp->rx_lock);
		break;
	}
	case HDMI_IOC_HDCP_ON:
		hdcp_enable = 1;
		rx_set_cur_hpd(0);
		fsm_restart();
		break;
	case HDMI_IOC_HDCP_OFF:
		hdcp_enable = 0;
		rx_set_cur_hpd(0);
		hdmirx_hw_config();
		fsm_restart();
		break;
	case HDMI_IOC_EDID_UPDATE:
		hdmi_rx_ctrl_edid_update();
		rx.edid_load_flag = true;
		if (rx.open_fg) {
			rx_set_cur_hpd(0);
			edid_update_flag = 1;
		} else {
			if (is_meson_gxtvbb_cpu())
				hdmirx_wr_top(TOP_HPD_PWR5V, 0x1f);
			else
				hdmirx_wr_top(TOP_HPD_PWR5V, 0x10);
		}
		fsm_restart();
		rx_pr("*update edid*\n");
		break;
	case HDMI_IOC_PC_MODE_ON:
		pc_mode_en = 1;
		rx_pr("pc mode on\n");
		break;
	case HDMI_IOC_PC_MODE_OFF:
		pc_mode_en = 0;
		rx_pr("pc mode off\n");
		break;
	case HDMI_IOC_HDCP22_AUTO:
		rx_set_cur_hpd(0);
		hdcp22_on = 1;
		force_hdcp14_en = 0;
		hdmirx_hw_config();
		hpd_to_esm = 1;
		fsm_restart();
		rx_pr("hdcp22 auto\n");
		break;
	case HDMI_IOC_HDCP22_FORCE14:
		rx_set_cur_hpd(0);
		force_hdcp14_en = 1;
		hdcp22_on = 0;
		hdmirx_wr_dwc(DWC_HDCP22_CONTROL, 0x2);
		esm_set_stable(0);
		fsm_restart();
		rx_pr("force hdcp1.4\n");
		break;
	case HDMI_IOC_PD_FIFO_PKTTYPE_EN:
		/*rx_pr("IOC_PD_FIFO_PKTTYPE_EN\n");*/
		/*get input param*/
		if (copy_from_user(&param, argp, sizeof(uint32_t))) {
			pr_err("get pd fifo param err\n");
			ret = -EFAULT;
			break;
		}
		rx_pr("en cmd:0x%x\n", param);
		/*pd_fifo_start_cnt = 16;*/
		rx_pkt_buffclear(param);
		temp = rx_pkt_type_mapping(param);
		packet_fifo_cfg |= temp;
		/*enable pkt pd fifo*/
		temp_val = hdmirx_rd_dwc(DWC_PDEC_CTRL);
		temp_val |= temp;
		hdmirx_wr_dwc(DWC_PDEC_CTRL, temp_val);
		/*enable int*/
		pdec_ists_en |= PD_FIFO_START_PASS|PD_FIFO_OVERFL;
		/*open pd fifo interrupt source if signal stable*/
		temp_val = hdmirx_rd_dwc(DWC_PDEC_IEN);
		if (((temp_val&PD_FIFO_START_PASS) == 0) &&
			(rx.state >= FSM_SIG_UNSTABLE)) {
			temp_val |= pdec_ists_en;
			hdmirx_wr_dwc(DWC_PDEC_IEN_SET, temp_val);
			rx_pr("open pd fifo int:0x%x\n", pdec_ists_en);
		}
		break;
	case HDMI_IOC_PD_FIFO_PKTTYPE_DIS:
		/*rx_pr("IOC_PD_FIFO_PKTTYPE_DIS\n");*/
		/*get input param*/
		if (copy_from_user(&param, argp, sizeof(uint32_t))) {
			pr_err("get pd fifo param err\n");
			ret = -EFAULT;
			break;
		}
		rx_pr("dis cmd:0x%x\n", param);
		temp = rx_pkt_type_mapping(param);
		packet_fifo_cfg &= ~temp;
		/*disable pkt pd fifo*/
		temp_val = hdmirx_rd_dwc(DWC_PDEC_CTRL);
		temp_val &= ~temp;
		hdmirx_wr_dwc(DWC_PDEC_CTRL, temp_val);
		break;

	case HDMI_IOC_GET_PD_FIFO_PARAM:
		/*mutex_lock(&pktbuff_lock);*/
		/*rx_pr("IOC_GET_PD_FIFO_PARAM\n");*/
		/*get input param*/
		if (copy_from_user(&param, argp, sizeof(uint32_t))) {
			pr_err("get pd fifo param err\n");
			ret = -EFAULT;
			/*mutex_unlock(&pktbuff_lock);*/
			break;
		}
		memset(&pkt_info, 0, sizeof(pkt_info));
		srcbuff = &pkt_info;
		size = sizeof(struct pd_infoframe_s);
		switch (param) {
		case PKT_TYPE_INFOFRAME_VSI:
			if (rx_pkt_get_fifo_pri())
				srcbuff = &rx.vs_info;
			else
				rx_pkt_get_vsi_ex(&pkt_info);
			break;
		case PKT_TYPE_INFOFRAME_AVI:
			if (rx_pkt_get_fifo_pri())
				srcbuff = &rx.avi_info;
			else
				rx_pkt_get_avi_ex(&pkt_info);
			break;
		case PKT_TYPE_INFOFRAME_SPD:
			srcbuff = &rx.spd_info;
			break;
		case PKT_TYPE_INFOFRAME_AUD:
			if (rx_pkt_get_fifo_pri())
				srcbuff = &rx.aud_pktinfo;
			else
				rx_pkt_get_audif_ex(&pkt_info);
			break;
		case PKT_TYPE_INFOFRAME_MPEGSRC:
			srcbuff = &rx.mpegs_info;
			break;
		case PKT_TYPE_INFOFRAME_NVBI:
			if (rx_pkt_get_fifo_pri())
				srcbuff = &rx.ntscvbi_info;
			else
				rx_pkt_get_ntscvbi_ex(&pkt_info);
			break;
		case PKT_TYPE_INFOFRAME_DRM:
			if (rx_pkt_get_fifo_pri())
				srcbuff = &rx.drm_info;
			else
				rx_pkt_get_drm_ex(&pkt_info);
			break;
		case PKT_TYPE_ACR:
			if (rx_pkt_get_fifo_pri())
				srcbuff = &rx.acr_info;
			else
				rx_pkt_get_acr_ex(&pkt_info);
			break;
		case PKT_TYPE_GCP:
			if (rx_pkt_get_fifo_pri())
				srcbuff = &rx.gcp_info;
			else
				rx_pkt_get_gcp_ex(&pkt_info);
			break;
		case PKT_TYPE_ACP:
			srcbuff = &rx.acp_info;
			break;
		case PKT_TYPE_ISRC1:
			srcbuff = &rx.isrc1_info;
			break;
		case PKT_TYPE_ISRC2:
			srcbuff = &rx.isrc2_info;
			break;
		case PKT_TYPE_GAMUT_META:
			if (rx_pkt_get_fifo_pri())
				srcbuff = &rx.gameta_info;
			else
				rx_pkt_get_gmd_ex(&pkt_info);
			break;
		case PKT_TYPE_AUD_META:
			if (rx_pkt_get_fifo_pri())
				srcbuff = &rx.amp_info;
			else
				rx_pkt_get_amp_ex(&pkt_info);
			break;
		default:
			size = PFIFO_SIZE * sizeof(uint32_t);
			if (pd_fifo_buf != NULL)
				srcbuff = pd_fifo_buf;
			else {
				size = 0;
				pr_err("err:pd_fifo_buf is empty\n");
			}
			break;
		}
		/*return pkt info*/
		if ((size > 0) && (srcbuff != NULL) && (argp != NULL)) {
			if (copy_to_user(argp, srcbuff, size)) {
				pr_err("get pd fifo param err\n");
				ret = -EFAULT;
			}
		}
		/*mutex_unlock(&pktbuff_lock);*/
		break;
	default:
		ret = -ENOIOCTLCMD;
		break;
	}
	return ret;
}

#ifdef CONFIG_COMPAT
static long hdmirx_compat_ioctl(struct file *file, unsigned int cmd,
	unsigned long arg)
{
	unsigned long ret;
	arg = (unsigned long)compat_ptr(arg);
	ret = hdmirx_ioctl(file, cmd, arg);
	return ret;
}
#endif

void hotplug_wait_query(void)
{
	wake_up(&query_wait);
}

static ssize_t hdmirx_hpd_read(struct file *file,
	    char __user *buf, size_t count, loff_t *pos)
{
	int ret = 0;
	unsigned int rx_sts;

	rx_sts = pwr_sts | (aspect_ratio_val << 4);
	if (copy_to_user(buf, &rx_sts, sizeof(unsigned int)))
		return -EFAULT;
	if (log_level & VIDEO_LOG)
			rx_pr("aspect event=%d\n", aspect_ratio_val);
	return ret;
}

static unsigned int hdmirx_hpd_poll(struct file *filp,
		poll_table *wait)
{
	unsigned int mask = 0;

	poll_wait(filp, &query_wait, wait);
	mask |= POLLIN|POLLRDNORM;

	return mask;
}

static const struct file_operations hdmirx_fops = {
	.owner		= THIS_MODULE,
	.open		= hdmirx_open,
	.release	= hdmirx_release,
	.read       = hdmirx_hpd_read,
	.poll       = hdmirx_hpd_poll,
	.unlocked_ioctl	= hdmirx_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl = hdmirx_compat_ioctl,
#endif
};

int rx_pr_buf(char *buf, int len)
{
	unsigned long flags;
	int pos;
	int hdmirx_log_rd_pos_;

	if (hdmirx_log_buf_size == 0)
		return 0;
	spin_lock_irqsave(&rx_pr_lock, flags);
	hdmirx_log_rd_pos_ = hdmirx_log_rd_pos;
	if (hdmirx_log_wr_pos >= hdmirx_log_rd_pos)
		hdmirx_log_rd_pos_ += hdmirx_log_buf_size;
	for (pos = 0;
		pos < len && hdmirx_log_wr_pos < (hdmirx_log_rd_pos_ - 1);
		pos++, hdmirx_log_wr_pos++) {
		if (hdmirx_log_wr_pos >= hdmirx_log_buf_size)
			hdmirx_log_buf[hdmirx_log_wr_pos - hdmirx_log_buf_size]
				= buf[pos];
		else
			hdmirx_log_buf[hdmirx_log_wr_pos] = buf[pos];
	}
	if (hdmirx_log_wr_pos >= hdmirx_log_buf_size)
		hdmirx_log_wr_pos -= hdmirx_log_buf_size;
	spin_unlock_irqrestore(&rx_pr_lock, flags);
	return pos;
}

int rx_pr(const char *fmt, ...)
{
	va_list args;
	int avail = PRINT_TEMP_BUF_SIZE;
	char buf[PRINT_TEMP_BUF_SIZE];
	int pos = 0;
	int len = 0;
	static bool last_break = 1;
	if ((last_break == 1) &&
		(strlen(fmt) > 1)) {
		strcpy(buf, "[RX]-");
		for (len = 0; len < strlen(fmt); len++)
			if (fmt[len] == '\n')
				pos++;
			else
				break;

		strncpy(buf + 5, fmt + pos, (sizeof(buf) - 5));
	} else
		strcpy(buf, fmt);
	if (fmt[strlen(fmt) - 1] == '\n')
		last_break = 1;
	else
		last_break = 0;
	if (log_level & LOG_EN) {
		va_start(args, fmt);
		vprintk(buf, args);
		va_end(args);
		return 0;
	}
	if (hdmirx_log_buf_size == 0)
		return 0;

	/* len += snprintf(buf+len, avail-len, "%d:",log_seq++); */
	len += snprintf(buf + len, avail - len, "[%u] ", (unsigned int)jiffies);
	va_start(args, fmt);
	len += vsnprintf(buf + len, avail - len, fmt, args);
	va_end(args);
	if ((avail-len) <= 0)
		buf[PRINT_TEMP_BUF_SIZE - 1] = '\0';

	pos = rx_pr_buf(buf, len);
	return pos;
}

static int log_init(int bufsize)
{
	if (bufsize == 0) {
		if (hdmirx_log_buf) {
			/* kfree(hdmirx_log_buf); */
			hdmirx_log_buf = NULL;
			hdmirx_log_buf_size = 0;
			hdmirx_log_rd_pos = 0;
			hdmirx_log_wr_pos = 0;
		}
	}
	if ((bufsize >= 1024) && (hdmirx_log_buf == NULL)) {
		hdmirx_log_buf_size = 0;
		hdmirx_log_rd_pos = 0;
		hdmirx_log_wr_pos = 0;
		hdmirx_log_buf = kmalloc(bufsize, GFP_KERNEL);
		if (hdmirx_log_buf)
			hdmirx_log_buf_size = bufsize;
	}
	return 0;
}

static ssize_t show_log(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	unsigned long flags;
	ssize_t read_size = 0;

	if (hdmirx_log_buf_size == 0)
		return 0;
	spin_lock_irqsave(&rx_pr_lock, flags);
	if (hdmirx_log_rd_pos < hdmirx_log_wr_pos)
		read_size = hdmirx_log_wr_pos-hdmirx_log_rd_pos;
	else if (hdmirx_log_rd_pos > hdmirx_log_wr_pos)
		read_size = hdmirx_log_buf_size-hdmirx_log_rd_pos;

	if (read_size > PAGE_SIZE)
		read_size = PAGE_SIZE;
	if (read_size > 0)
		memcpy(buf, hdmirx_log_buf+hdmirx_log_rd_pos, read_size);
	hdmirx_log_rd_pos += read_size;
	if (hdmirx_log_rd_pos >= hdmirx_log_buf_size)
		hdmirx_log_rd_pos = 0;
	spin_unlock_irqrestore(&rx_pr_lock, flags);
	return read_size;
}

static ssize_t store_log(struct device *dev,
	struct device_attribute *attr,
	const char *buf, size_t count)
{
	long tmp;
	unsigned long flags;

	if (strncmp(buf, "bufsize", 7) == 0) {
		if (kstrtoul(buf + 7, 10, &tmp) < 0)
			return -EINVAL;
		spin_lock_irqsave(&rx_pr_lock, flags);
		log_init(tmp);
		spin_unlock_irqrestore(&rx_pr_lock, flags);
		rx_pr("hdmirx_store:set bufsize tmp %ld %d\n",
			tmp, hdmirx_log_buf_size);
	} else {
		rx_pr(0, "%s", buf);
	}
	return 16;
}


static ssize_t hdmirx_debug_show(struct device *dev,
	struct device_attribute *attr,
	char *buf)
{
	return 0;
}

static ssize_t hdmirx_debug_store(struct device *dev,
	struct device_attribute *attr,
	const char *buf,
	size_t count)
{
	hdmirx_debug(buf, count);
	return count;
}

static ssize_t hdmirx_edid_show(struct device *dev,
	struct device_attribute *attr,
	char *buf)
{
	return hdmirx_read_edid_buf(buf, PAGE_SIZE);
}

static ssize_t hdmirx_edid_store(struct device *dev,
	struct device_attribute *attr,
	const char *buf,
	size_t count)
{
	hdmirx_fill_edid_buf(buf, count);
	return count;
}

static ssize_t hdmirx_key_show(struct device *dev,
	struct device_attribute *attr,
	char *buf)
{
	return hdmirx_read_key_buf(buf, PAGE_SIZE);
}

static ssize_t hdmirx_key_store(struct device *dev,
	struct device_attribute *attr,
	const char *buf,
	size_t count)
{
	hdmirx_fill_key_buf(buf, count);
	return count;
}

static ssize_t show_reg(struct device *dev,
	struct device_attribute *attr,
	char *buf)
{
	return hdmirx_hw_dump_reg(buf, PAGE_SIZE);
}

static ssize_t cec_get_state(struct device *dev,
	struct device_attribute *attr,
	char *buf)
{
	return 0;
}

static ssize_t cec_set_state(struct device *dev,
	struct device_attribute *attr,
	const char *buf,
	size_t count)
{
	return count;
}

static ssize_t param_get_value(struct device *dev,
	struct device_attribute *attr,
	char *buf)
{
	rx_get_global_varaible(buf);
	return 0;
}

static ssize_t param_set_value(struct device *dev,
	struct device_attribute *attr,
	const char *buf,
	size_t count)
{
	rx_set_global_varaible(buf, count);
	return count;
}

static ssize_t esm_get_base(struct device *dev,
struct device_attribute *attr,
char *buf)
{
int pos = 0;

pos += snprintf(buf, PAGE_SIZE, "0x%x\n",
	reg_maps[rx.chip_id][MAP_ADDR_MODULE_HDMIRX_CAPB3].phy_addr);
rx_pr("hdcp_rx22 get esm base:%#x\n",
	reg_maps[rx.chip_id][MAP_ADDR_MODULE_HDMIRX_CAPB3].phy_addr);

return pos;
}

static ssize_t esm_set_base(struct device *dev,
struct device_attribute *attr,
const char *buf,
size_t count)
{
return count;
}

static ssize_t show_info(struct device *dev,
	struct device_attribute *attr,
	char *buf)
{
	return hdmirx_show_info(buf, PAGE_SIZE);
}

static ssize_t store_info(struct device *dev,
	struct device_attribute *attr,
	const char *buf,
	size_t count)
{
	return count;
}

static DEVICE_ATTR(debug, 0664, hdmirx_debug_show, hdmirx_debug_store);
static DEVICE_ATTR(edid, 0664, hdmirx_edid_show, hdmirx_edid_store);
static DEVICE_ATTR(key, 0664, hdmirx_key_show, hdmirx_key_store);
static DEVICE_ATTR(log, 0664, show_log, store_log);
static DEVICE_ATTR(reg, 0664, show_reg, store_log);
static DEVICE_ATTR(cec, 0664, cec_get_state, cec_set_state);
static DEVICE_ATTR(param, 0664, param_get_value, param_set_value);
static DEVICE_ATTR(esm_base, 0664, esm_get_base, esm_set_base);
static DEVICE_ATTR(info, 0664, show_info, store_info);
#ifdef CONFIG_AM_HDMI_SCAN_MODE_NODE
static DEVICE_ATTR(scan_mode, 0444, scan_mode_show, NULL);
#endif

static int hdmirx_add_cdev(struct cdev *cdevp,
		const struct file_operations *fops,
		int minor)
{
	int ret;
	dev_t devno = MKDEV(MAJOR(hdmirx_devno), minor);

	cdev_init(cdevp, fops);
	cdevp->owner = THIS_MODULE;
	ret = cdev_add(cdevp, devno, 1);
	return ret;
}

static struct device *hdmirx_create_device(struct device *parent, int id)
{
	dev_t devno = MKDEV(MAJOR(hdmirx_devno),  id);
	return device_create(hdmirx_clsp, parent, devno, NULL, "%s0",
			TVHDMI_DEVICE_NAME);
	/* @to do this after Middleware API modified */
	/*return device_create(hdmirx_clsp, parent, devno, NULL, "%s",
	  TVHDMI_DEVICE_NAME); */
}

static void hdmirx_delete_device(int minor)
{
	dev_t devno = MKDEV(MAJOR(hdmirx_devno), minor);
	device_destroy(hdmirx_clsp, devno);
}

static void hdmirx_get_base_addr(struct device_node *node)
{
	int ret;
	struct device_node *node_sub = NULL;
	int reg_arry[2];

	/*get base addr from dts*/
	if (node)
		node_sub = of_get_child_by_name(node, "hdmirx_port");
	rx_pr("node_p = %p\n", node_sub);
	if (node_sub) {
		ret = of_property_read_u32_array(node_sub,
				"reg", reg_arry, 2);
		rx_pr("hdmirx_port:%#x,%#x\n", reg_arry[0], reg_arry[1]);
		if (!ret && reg_arry[0]) {
			hdmirx_addr_port = reg_arry[0];
			reg_maps[rx.chip_id][MAP_ADDR_MODULE_TOP].phy_addr =
				reg_arry[0];
			reg_maps[rx.chip_id][MAP_ADDR_MODULE_TOP].size =
				reg_arry[1];
		}
	}
	if (node)
		node_sub = of_get_child_by_name(node, "hiu_io");
	rx_pr("node_p = %p\n", node_sub);
	if (node_sub) {
		ret = of_property_read_u32_array(node_sub,
				"reg", reg_arry, 2);
		rx_pr("hiu addr:%#x,%#x\n", reg_arry[0], reg_arry[1]);
		if (!ret && reg_arry[0]) {
			reg_maps[rx.chip_id][MAP_ADDR_MODULE_HIU].phy_addr =
				reg_arry[0];
			reg_maps[rx.chip_id][MAP_ADDR_MODULE_HIU].size =
				reg_arry[1];
		}
	}
	rx_pr("hiu_base_addr:%#x\n",
			reg_maps[rx.chip_id][MAP_ADDR_MODULE_HIU].phy_addr);
	if (node) {
		if (hdmirx_addr_port == 0) {
			ret = of_property_read_u32(node,
				"hdmirx_addr_port", &hdmirx_addr_port);
			if (ret)
				pr_err("get hdmirx_addr_port fail.\n");

			ret = of_property_read_u32(node,
					"hdmirx_data_port", &hdmirx_data_port);
			if (ret)
				pr_err("get hdmirx_data_port fail.\n");
			ret = of_property_read_u32(node,
					"hdmirx_ctrl_port", &hdmirx_ctrl_port);
			if (ret)
				pr_err("get hdmirx_ctrl_port fail.\n");
		} else {
			hdmirx_data_port = hdmirx_addr_port + 4;
			hdmirx_ctrl_port = hdmirx_data_port + 8;
		}
		rx_pr("port addr:%#x ,data:%#x, ctrl:%#x\n",
			hdmirx_addr_port, hdmirx_data_port, hdmirx_ctrl_port);
	}
	reg_maps[rx.chip_id][MAP_ADDR_MODULE_TOP].phy_addr = hdmirx_addr_port;
}

static int hdmirx_switch_pinmux(struct device *dev)
{
	struct pinctrl *pin;
	const char *pin_name;
	int ret = 0;

	/* pinmux set */
	if (dev->of_node) {
		ret = of_property_read_string_index(dev->of_node,
					    "pinctrl-names",
					    0, &pin_name);
		if (!ret) {
			pin = devm_pinctrl_get_select(dev, pin_name);
			rx_pr("hdmirx: pinmux:%p, name:%s\n", pin, pin_name);
		}
	}
	return ret;
}


static int hdmirx_probe(struct platform_device *pdev)
{
	int ret = 0;
	struct hdmirx_dev_s *hdevp;
	struct resource *res;
	struct clk *xtal_clk;
	struct clk *fclk_div5_clk;
	struct clk *tmds_clk_fs;
	int clk_rate;

	rx.chip_id = get_chip_id();
	log_init(DEF_LOG_BUF_SIZE);
	pEdid_buffer = (unsigned char *) pdev->dev.platform_data;
	hdmirx_dev = &pdev->dev;
	/* allocate memory for the per-device structure */
	hdevp = kmalloc(sizeof(struct hdmirx_dev_s), GFP_KERNEL);
	if (!hdevp) {
		rx_pr("hdmirx:allocate memory failed\n");
		ret = -ENOMEM;
		goto fail_kmalloc_hdev;
	}
	memset(hdevp, 0, sizeof(struct hdmirx_dev_s));

	hdmirx_get_base_addr(pdev->dev.of_node);
	rx_init_reg_map();
	/*@to get from bsp*/
	#if 0
	if (pdev->dev.of_node) {
		ret = of_property_read_u32(pdev->dev.of_node,
				"hdmirx_id", &(hdevp->index));
		if (ret) {
			pr_err("%s:don't find  hdmirx id.\n", __func__);
			goto fail_create_device;
		}
	} else {
			pr_err("%s: don't find match hdmirx node\n", __func__);
			return -1;
	}
	#endif
	hdevp->index = 0; /* pdev->id; */
	/* create cdev and reigser with sysfs */
	ret = hdmirx_add_cdev(&hdevp->cdev, &hdmirx_fops, hdevp->index);
	if (ret) {
		rx_pr("%s: failed to add cdev\n", __func__);
		goto fail_add_cdev;
	}
	/* create /dev nodes */
	hdevp->dev = hdmirx_create_device(&pdev->dev, hdevp->index);
	if (IS_ERR(hdevp->dev)) {
		rx_pr("hdmirx: failed to create device node\n");
		ret = PTR_ERR(hdevp->dev);
		goto fail_create_device;
	}
	/*create sysfs attribute files*/
	ret = device_create_file(hdevp->dev, &dev_attr_debug);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create debug attribute file\n");
		goto fail_create_debug_file;
	}
	ret = device_create_file(hdevp->dev, &dev_attr_edid);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create edid attribute file\n");
		goto fail_create_edid_file;
	}
	ret = device_create_file(hdevp->dev, &dev_attr_key);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create key attribute file\n");
		goto fail_create_key_file;
	}
	ret = device_create_file(hdevp->dev, &dev_attr_log);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create log attribute file\n");
		goto fail_create_log_file;
	}
	ret = device_create_file(hdevp->dev, &dev_attr_reg);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create reg attribute file\n");
		goto fail_create_reg_file;
	}
	ret = device_create_file(hdevp->dev, &dev_attr_param);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create param attribute file\n");
		goto fail_create_param_file;
	}
	ret = device_create_file(hdevp->dev, &dev_attr_info);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create info attribute file\n");
		goto fail_create_info_file;
	}
	ret = device_create_file(hdevp->dev, &dev_attr_esm_base);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create esm_base attribute file\n");
		goto fail_create_esm_base_file;
	}
	ret = device_create_file(hdevp->dev, &dev_attr_cec);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create cec attribute file\n");
		goto fail_create_cec_file;
	}
#ifdef CONFIG_AM_HDMI_SCAN_MODE_NODE
	ret = device_create_file(hdevp->dev, &dev_attr_scan_mode);
	if (ret < 0) {
		rx_pr("hdmirx: fail to create scan_mode file\n");
		goto fail_create_scan_mode;
	}
#endif
	res = platform_get_resource(pdev, IORESOURCE_IRQ, 0);
	if (!res) {
		rx_pr("%s: can't get irq resource\n", __func__);
		ret = -ENXIO;
		goto fail_get_resource_irq;
	}
	hdevp->irq = res->start;
	snprintf(hdevp->irq_name, sizeof(hdevp->irq_name),
			"hdmirx%d-irq", hdevp->index);
	rx_pr("hdevpd irq: %d, %d\n", hdevp->index,
			hdevp->irq);
	if (pdev->dev.of_node) {
		ret = of_property_read_u32(pdev->dev.of_node,
				"repeat", &repeat_function);
		if (ret) {
			pr_err("get repeat_function fail.\n");
			repeat_function = 0;
		}
	}
	rx.hdcp.switch_hdcp_auth.name = "hdmirx_hdcp_auth";
	ret = switch_dev_register(&rx.hdcp.switch_hdcp_auth);
	if (ret)
		pr_err("hdcp_auth switch init fail.\n");
	rx.hpd_sdev.name = "hdmirx_hpd";
	ret = switch_dev_register(&rx.hpd_sdev);
	if (ret)
		pr_err("hdmirx_hpd switch init fail.\n");
	if (request_irq(hdevp->irq,
			&irq_handler,
			IRQF_SHARED,
			hdevp->irq_name,
			(void *)&rx))
		rx_pr(__func__, "RX IRQ request");
	/* frontend */
	tvin_frontend_init(&hdevp->frontend,
		&hdmirx_dec_ops,
		&hdmirx_sm_ops,
		hdevp->index);
	sprintf(hdevp->frontend.name, "%s", TVHDMI_NAME);
	if (tvin_reg_frontend(&hdevp->frontend) < 0)
		rx_pr("hdmirx: driver probe error!!!\n");

	dev_set_drvdata(hdevp->dev, hdevp);
	platform_set_drvdata(pdev, hdevp);

	xtal_clk = clk_get(&pdev->dev, "xtal");
	if (IS_ERR(xtal_clk))
		rx_pr("get xtal err\n");
	else {
		clk_rate = clk_get_rate(xtal_clk);
		pr_info("%s: xtal_clk is %d MHZ\n", __func__,
				clk_rate/1000000);
	}
	fclk_div5_clk = clk_get(&pdev->dev, "fclk_div5");
	if (IS_ERR(fclk_div5_clk))
		rx_pr("get fclk_div5_clk err\n");
	else {
		clk_rate = clk_get_rate(fclk_div5_clk);
		pr_info("%s: fclk_div5_clk is %d MHZ\n", __func__,
				clk_rate/1000000);
	}
	hdevp->modet_clk = clk_get(&pdev->dev, "hdmirx_modet_clk");
	if (IS_ERR(hdevp->modet_clk))
		rx_pr("get modet_clk err\n");
	else {
		clk_set_parent(hdevp->modet_clk, xtal_clk);
		clk_set_rate(hdevp->modet_clk, 24000000);
		clk_rate = clk_get_rate(hdevp->modet_clk);
		pr_info("%s: modet_clk is %d MHZ\n", __func__,
				clk_rate/1000000);
	}

	hdevp->cfg_clk = clk_get(&pdev->dev, "hdmirx_cfg_clk");
	if (IS_ERR(hdevp->cfg_clk))
		rx_pr("get cfg_clk err\n");
	else {
		clk_set_parent(hdevp->cfg_clk, fclk_div5_clk);
		clk_set_rate(hdevp->cfg_clk, 133333333);
		clk_rate = clk_get_rate(hdevp->cfg_clk);
		pr_info("%s: cfg_clk is %d MHZ\n", __func__,
				clk_rate/1000000);
	}

	if (is_meson_txlx_cpu()) {
		tmds_clk_fs = clk_get(&pdev->dev, "hdmirx_aud_pll2fs");
		if (IS_ERR(tmds_clk_fs))
			rx_pr("get tmds_clk_fs err\n");
		hdevp->aud_out_clk = clk_get(&pdev->dev, "clk_aud_out");
		if (IS_ERR(hdevp->aud_out_clk) || IS_ERR(tmds_clk_fs))
			rx_pr("get aud_out_clk/tmds_clk_fs err\n");
		else {
			clk_set_parent(hdevp->aud_out_clk, tmds_clk_fs);
			clk_rate = clk_get_rate(hdevp->aud_out_clk);
			pr_info("%s: aud_out_clk is %d MHZ\n", __func__,
				clk_rate/1000000);
		}
	}


	/*
	hdevp->acr_ref_clk = clk_get(&pdev->dev, "hdmirx_acr_ref_clk");
	if (IS_ERR(hdevp->acr_ref_clk))
		rx_pr("get acr_ref_clk err\n");
	else {
		clk_set_parent(hdevp->acr_ref_clk, fclk_div5_clk);
		clk_set_rate(hdevp->acr_ref_clk, 24000000);
		clk_rate = clk_get_rate(hdevp->acr_ref_clk);
		pr_info("%s: acr_ref_clk is %d MHZ\n", __func__,
				clk_rate/1000000);
	}
	*/
	hdevp->audmeas_clk = clk_get(&pdev->dev, "hdmirx_audmeas_clk");
	if (IS_ERR(hdevp->audmeas_clk))
		rx_pr("get audmeas_clk err\n");
	else {
		clk_set_parent(hdevp->audmeas_clk, fclk_div5_clk);
		clk_set_rate(hdevp->audmeas_clk, 200000000);
		clk_rate = clk_get_rate(hdevp->audmeas_clk);
		pr_info("%s: audmeas_clk is %d MHZ\n", __func__,
				clk_rate/1000000);
	}

	pd_fifo_buf = kmalloc(PFIFO_SIZE * sizeof(uint32_t), GFP_KERNEL);
	if (!pd_fifo_buf) {
		rx_pr("hdmirx:allocate pd fifo failed\n");
		ret = -ENOMEM;
		goto fail_kmalloc_pd_fifo;
	}

	tasklet_init(&rx_tasklet, rx_tasklet_handler, (unsigned long)&rx);
	/* create for hot plug function */
	eq_wq = create_singlethread_workqueue(hdevp->frontend.name);
	INIT_DELAYED_WORK(&eq_dwork, eq_algorithm);

	esm_wq = create_singlethread_workqueue(hdevp->frontend.name);
	INIT_DELAYED_WORK(&esm_dwork, rx_hpd_to_esm_handle);
	/* queue_delayed_work(eq_wq, &eq_dwork, msecs_to_jiffies(5)); */

	repeater_wq = create_singlethread_workqueue(hdevp->frontend.name);
	INIT_DELAYED_WORK(&repeater_dwork, repeater_dwork_handle);

	ret = of_property_read_u32(pdev->dev.of_node,
				"en_4k_2_2k", &en_4k_2_2k);
	if (ret) {
		pr_err("%s:don't find  en_4k_2_2k.\n", __func__);
		en_4k_2_2k = 0;
	}

	ret = of_property_read_u32(pdev->dev.of_node,
				"en_4k_timing", &en_4k_timing);
	if (ret) {
		pr_err("%s:don't find  en_4k_timing.\n", __func__);
		en_4k_timing = 1;
	}
	ret = of_property_read_u32(pdev->dev.of_node,
				"arc_port", &arc_port_id);
	if (ret) {
		pr_err("arc_port not found\n");
	}
	hdmirx_hw_probe();
	hdmirx_switch_pinmux(&(pdev->dev));

	mutex_init(&hdevp->rx_lock);
	register_pm_notifier(&aml_hdcp22_pm_notifier);

	init_timer(&hdevp->timer);
	hdevp->timer.data = (ulong)hdevp;
	hdevp->timer.function = hdmirx_timer_handler;
	hdevp->timer.expires = jiffies + TIMER_STATE_CHECK;
	add_timer(&hdevp->timer);
	rx.boot_flag = TRUE;
	rx_pr("hdmirx: driver probe ok\n");

	return 0;
#ifdef CONFIG_AM_HDMI_SCAN_MODE_NODE
fail_create_scan_mode:
	device_remove_file(hdevp->dev, &dev_attr_scan_mode);
#endif
fail_create_cec_file:
		if (!is_meson_txlx_cpu())
			device_remove_file(hdevp->dev, &dev_attr_cec);
fail_create_esm_base_file:
	device_remove_file(hdevp->dev, &dev_attr_esm_base);
fail_create_reg_file:
	device_remove_file(hdevp->dev, &dev_attr_reg);
fail_create_log_file:
	device_remove_file(hdevp->dev, &dev_attr_log);
fail_create_key_file:
	device_remove_file(hdevp->dev, &dev_attr_key);
fail_create_edid_file:
	device_remove_file(hdevp->dev, &dev_attr_edid);
fail_create_debug_file:
	device_remove_file(hdevp->dev, &dev_attr_debug);
fail_create_param_file:
	device_remove_file(hdevp->dev, &dev_attr_param);
fail_create_info_file:
		device_remove_file(hdevp->dev, &dev_attr_info);

/* fail_get_resource_irq: */
	/* hdmirx_delete_device(hdevp->index); */
fail_create_device:
	cdev_del(&hdevp->cdev);
fail_add_cdev:
/* fail_get_id: */
	kfree(hdevp);
fail_kmalloc_hdev:
	return ret;
fail_kmalloc_pd_fifo:
	return ret;
fail_get_resource_irq:
	return ret;
}

static int hdmirx_remove(struct platform_device *pdev)
{
	struct hdmirx_dev_s *hdevp;

	hdevp = platform_get_drvdata(pdev);

	cancel_delayed_work_sync(&eq_dwork);
	destroy_workqueue(eq_wq);

	cancel_delayed_work_sync(&esm_dwork);
	destroy_workqueue(esm_wq);

	mutex_destroy(&hdevp->rx_lock);
#ifdef CONFIG_AM_HDMI_SCAN_MODE_NODE
	device_remove_file(hdevp->dev, &dev_attr_scan_mode);
#endif
	device_remove_file(hdevp->dev, &dev_attr_debug);
	device_remove_file(hdevp->dev, &dev_attr_edid);
	device_remove_file(hdevp->dev, &dev_attr_key);
	device_remove_file(hdevp->dev, &dev_attr_log);
	device_remove_file(hdevp->dev, &dev_attr_reg);
	device_remove_file(hdevp->dev, &dev_attr_cec);
	device_remove_file(hdevp->dev, &dev_attr_esm_base);
	device_remove_file(hdevp->dev, &dev_attr_info);
	tvin_unreg_frontend(&hdevp->frontend);
	hdmirx_delete_device(hdevp->index);
	tasklet_kill(&rx_tasklet);
	kfree(pd_fifo_buf);
	cdev_del(&hdevp->cdev);
	kfree(hdevp);
	rx_pr("hdmirx: driver removed ok.\n");
	return 0;
}


static int aml_hdcp22_pm_notify(struct notifier_block *nb, unsigned long event,
		void *dummy)
{
	int delay = 0;

	if (event == PM_SUSPEND_PREPARE && hdcp22_on) {
		hdcp22_kill_esm = 1;
		/*wait time out ESM_KILL_WAIT_TIMES*20 ms*/
		while (delay++ < ESM_KILL_WAIT_TIMES) {
			if (!hdcp22_kill_esm)
				break;
			msleep(20);
		}
		if (delay < ESM_KILL_WAIT_TIMES)
			rx_pr("hdcp22 kill ok!\n");
		else
			rx_pr("hdcp22 kill timeout!\n");
	}
	return NOTIFY_OK;
}

#ifdef CONFIG_PM
static int hdmirx_suspend(struct platform_device *pdev, pm_message_t state)
{
	struct hdmirx_dev_s *hdevp;
	hdevp = platform_get_drvdata(pdev);
	rx_pr("[hdmirx]: hdmirx_suspend\n");
	del_timer_sync(&hdevp->timer);

	/* phy powerdown */
	hdmirx_phy_pddq(1);
	if (hdcp22_on)
		hdcp22_suspend();
	rx_pr("[hdmirx]: suspend success\n");
	return 0;
}

static int hdmirx_resume(struct platform_device *pdev)
{
	struct hdmirx_dev_s *hdevp;
	hdevp = platform_get_drvdata(pdev);
	hdmirx_phy_init();
	add_timer(&hdevp->timer);
	if (hdcp22_on)
		hdcp22_resume();
	rx_pr("hdmirx: resume\n");
	pre_port = 0xff;
	rx.boot_flag = TRUE;
	return 0;
}
#endif

static void hdmirx_shutdown(struct platform_device *pdev)
{
	struct hdmirx_dev_s *hdevp;
	hdevp = platform_get_drvdata(pdev);
	rx_pr("[hdmirx]: hdmirx_shutdown\n");
	del_timer_sync(&hdevp->timer);
	/* phy powerdown */
	hdmirx_phy_pddq(1);
	if (hdcp22_on)
		hdcp22_clk_en(0);
	rx_pr("[hdmirx]: shutdown success\n");
}

#ifdef CONFIG_HIBERNATION
static int hdmirx_restore(struct device *dev)
{
	/* queue_delayed_work(eq_wq, &eq_dwork, msecs_to_jiffies(5)); */
	return 0;
}
static int hdmirx_pm_suspend(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	return hdmirx_suspend(pdev, PMSG_SUSPEND);
}

static int hdmirx_pm_resume(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	return hdmirx_resume(pdev);
}

const struct dev_pm_ops hdmirx_pm = {
	.restore	= hdmirx_restore,
	.suspend	= hdmirx_pm_suspend,
	.resume		= hdmirx_pm_resume,
};

#endif

static struct platform_driver hdmirx_driver = {
	.probe      = hdmirx_probe,
	.remove     = hdmirx_remove,
#ifdef CONFIG_PM
	.suspend    = hdmirx_suspend,
	.resume     = hdmirx_resume,
#endif
	.shutdown	= hdmirx_shutdown,
	.driver     = {
	.name   = TVHDMI_DRIVER_NAME,
	.owner	= THIS_MODULE,
	.of_match_table = hdmirx_dt_match,
#ifdef CONFIG_HIBERNATION
	.pm     = &hdmirx_pm,
#endif
	}
};

static int __init hdmirx_init(void)
{
	int ret = 0;
	/* struct platform_device *pdev; */

	if (init_flag & INIT_FLAG_NOT_LOAD)
		return 0;

	ret = alloc_chrdev_region(&hdmirx_devno, 0, 1, TVHDMI_NAME);
	if (ret < 0) {
		rx_pr("hdmirx: failed to allocate major number\n");
		goto fail_alloc_cdev_region;
	}

	hdmirx_clsp = class_create(THIS_MODULE, TVHDMI_NAME);
	if (IS_ERR(hdmirx_clsp)) {
		rx_pr("hdmirx: can't get hdmirx_clsp\n");
		ret = PTR_ERR(hdmirx_clsp);
		goto fail_class_create;
	}

	#if 0
	pdev = platform_device_alloc(TVHDMI_NAME, 0);
	if (IS_ERR(pdev)) {
		rx_pr("%s alloc platform device error.\n",
			__func__);
		goto fail_class_create;
	}
	if (platform_device_add(pdev)) {
		rx_pr("%s failed register platform device.\n",
			__func__);
		goto fail_class_create;
	}
	#endif
	ret = platform_driver_register(&hdmirx_driver);
	if (ret != 0) {
		rx_pr("register hdmirx module failed, error %d\n",
			ret);
		ret = -ENODEV;
		goto fail_pdrv_register;
	}
	rx_pr("hdmirx: hdmirx_init.\n");

	return 0;

fail_pdrv_register:
	class_destroy(hdmirx_clsp);
fail_class_create:
	unregister_chrdev_region(hdmirx_devno, 1);
fail_alloc_cdev_region:
	return ret;

}

static void __exit hdmirx_exit(void)
{
	class_destroy(hdmirx_clsp);
	unregister_chrdev_region(hdmirx_devno, 1);
	platform_driver_unregister(&hdmirx_driver);
	rx_pr("hdmirx: hdmirx_exit.\n");
}

module_init(hdmirx_init);
module_exit(hdmirx_exit);

MODULE_DESCRIPTION("AMLOGIC HDMIRX driver");
MODULE_LICENSE("GPL");
